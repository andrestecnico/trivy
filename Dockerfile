FROM ubuntu:18.04
RUN apt-get update -y \
    && apt-get install wget apt-transport-https gnupg lsb-release -y \
    && wget -qO - https://aquasecurity.github.io/trivy-repo/deb/public.key | apt-key add -  \
    && echo deb https://aquasecurity.github.io/trivy-repo/deb $(lsb_release -sc) main | tee /etc/apt/sources.list.d/trivy.list \
    && apt-get update -y \
    && apt-get install trivy -y 
CMD ["/bin/bash"]